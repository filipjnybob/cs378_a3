// Fill out your copyright notice in the Description page of Project Settings.


#include "TankController.h"
#include "Tank.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h"

ATankController::ATankController() 
{
    BehaviorComponent = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("Behavior Tree Component"));

    BlackboardComponent = CreateDefaultSubobject<UBlackboardComponent>(TEXT("Blackboard Component"));

	DestinationKey = "Destination";
	PlayerLocationKey = "PlayerLocation";
	SeePlayerKey = "CanSeePlayer";
}

void ATankController::SetPlayerCaught(APawn * InPawn)
{
	if (BlackboardComponent)
	{
		AActor* CurrentTarget = (AActor*)BlackboardComponent->GetValueAsObject(PlayerLocationKey);
		if (CurrentTarget) {
			FVector CurrentLocation = GetCharacter()->GetActorLocation();
			FVector TargetLocation = CurrentTarget->GetActorLocation();
			FVector NewTargetLocation = InPawn->GetActorLocation();

			//tank chases closest player

			if (FVector::Distance(CurrentLocation, NewTargetLocation) < FVector::Distance(CurrentLocation, TargetLocation)) {
				BlackboardComponent->SetValueAsObject(PlayerLocationKey, InPawn);
			}
		}
		else {
			BlackboardComponent->SetValueAsObject(PlayerLocationKey, InPawn);
		}
		

		BlackboardComponent->SetValueAsBool(SeePlayerKey, true);
	}
}

void ATankController::OnPossess(APawn *InPawn)
{
    Super::OnPossess(InPawn);

    ATank* Tank = Cast<ATank>(InPawn);

    if(BlackboardComponent) {
        if (Tank && Tank->GetBehaviorTree()) {
			if (Tank->GetBehaviorTree()->BlackboardAsset) {
				BlackboardComponent->InitializeBlackboard(*(Tank->GetBehaviorTree()->BlackboardAsset));
			}

			if (BehaviorComponent) {
				BehaviorComponent->StartTree(*Tank->GetBehaviorTree());
			}
		}
		
    }
}


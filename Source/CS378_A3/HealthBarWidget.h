// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HealthBarWidget.generated.h"

/**
 * 
 */
UCLASS()
class CS378_A3_API UHealthBarWidget : public UUserWidget
{
	GENERATED_BODY()

	UPROPERTY(meta = (BindWidget))
	class UProgressBar* HealthBar = nullptr;

public:
	UFUNCTION()
		void SetPercent(float Percent);

	UFUNCTION()
		float GetPercent();

	UFUNCTION()
		void SetVisible(bool Visible);
};

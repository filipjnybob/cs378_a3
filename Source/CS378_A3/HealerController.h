// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TankController.h"
#include "HealerController.generated.h"

/**
 * 
 */
UCLASS()
class CS378_A3_API AHealerController : public ATankController
{
	GENERATED_BODY()
	
public:
	AHealerController();

	virtual void SetPlayerCaught(APawn* InPawn) override;

	UFUNCTION()
		void Heal(AActor* Target);

	UPROPERTY(EditDefaultsOnly, Category = AI)
		FName HealTargetKey; // Key for the current heal target
};

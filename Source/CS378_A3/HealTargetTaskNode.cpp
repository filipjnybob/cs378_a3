// Fill out your copyright notice in the Description page of Project Settings.


#include "HealTargetTaskNode.h"
#include "HealerController.h"
#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UHealTargetTaskNode::ExecuteTask(UBehaviorTreeComponent& OwnerComponent, uint8* NodeMemory)
{
    AHealerController* AIController = Cast<AHealerController>(OwnerComponent.GetAIOwner());

    if (AIController)
    {
        UBlackboardComponent* BlackboardComponent = AIController->GetBlackboardComponent();

        AIController->Heal((AActor*)BlackboardComponent->GetValueAsObject(AIController->HealTargetKey));

        return EBTNodeResult::Succeeded;
    }
    return EBTNodeResult::Failed;
}
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_MoveTo.h"
#include "BTTask_MoveToRange.generated.h"

/**
 * 
 */
UCLASS()
class CS378_A3_API UBTTask_MoveToRange : public UBTTask_MoveTo
{
	GENERATED_BODY()
	
};

// Copyright Epic Games, Inc. All Rights Reserved.


#include "CS378_A3GameModeBase.h"
#include "FirstPersonCharacter.h"
#include "FirstPersonPlayerController.h"


ACS378_A3GameModeBase::ACS378_A3GameModeBase() {
	PlayerControllerClass = AFirstPersonPlayerController::StaticClass();

	static ConstructorHelpers::FObjectFinder<UClass> PawnBPClass(TEXT("Class'/Game/Blueprints/FirstPersonCharacterBP.FirstPersonCharacterBP_C'"));

	if (PawnBPClass.Object) {
		UClass* PawnBP = (UClass*)PawnBPClass.Object;
		DefaultPawnClass = PawnBP;
	}
	else {
		DefaultPawnClass = AFirstPersonCharacter::StaticClass();
	}

	bUseSeamlessTravel = true;
}


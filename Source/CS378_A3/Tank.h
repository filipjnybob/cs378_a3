// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Tank.generated.h"

UCLASS()
class CS378_A3_API ATank : public ACharacter
{
	GENERATED_BODY()

	UFUNCTION()
		void UpdateHealthBar();

public:
	// Sets default values for this character's properties
	ATank();

	FORCEINLINE class UBehaviorTree* GetBehaviorTree() const {return BehaviorTree;}
	FORCEINLINE class TArray<AActor*> GetPatrolPoints() const {return PatrolPoints;}

	FORCEINLINE class UCapsuleComponent* GetTriggerComponent() const {return TriggerComponent;}

	FORCEINLINE float GetHealth() const { return Health; }
	FORCEINLINE float GetMaxHealth() const { return MaxHealth; }

	UPROPERTY(VisibleAnywhere, Category = AI)
		class UPawnSensingComponent* PawnSensingComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UCapsuleComponent* TriggerComponent;

	UFUNCTION()
	 	void TakeDamage(float DamageValue);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		class UBehaviorTree* BehaviorTree;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<AActor*> PatrolPoints;

	UFUNCTION()
		void StartAttackPlayer(class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void EndAttackPlayer(class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		void DamagePlayer(class AFirstPersonCharacter* player);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float DamageDealt = 10;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MaxHealth = 100;

	UPROPERTY(ReplicatedUsing = OnRep_Health, EditAnywhere, BlueprintReadWrite)
		float Health = MaxHealth;

	UFUNCTION()
		void OnRep_Health();

	UPROPERTY()
		FTimerHandle AttackTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float AttackSpeed = 1.0f;

	UPROPERTY()
		bool bAttackPlayer = false;

	UFUNCTION()
		void EnemyDied();

	UFUNCTION(NetMulticast, Reliable)
		void NetMulticast_EnemyDied();
		void NetMulticast_EnemyDied_Implementation();

	UFUNCTION(Server, Reliable)
		void Server_EnemyDied();
		void Server_EnemyDied_Implementation();

	UPROPERTY()
		class UHealthBarWidget* HealthBar;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UWidgetComponent* WidgetComponent;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
protected:
	UFUNCTION()
	virtual void OnPlayerCaught(APawn* Pawn);



};

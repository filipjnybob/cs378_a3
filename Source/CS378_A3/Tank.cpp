// Fill out your copyright notice in the Description page of Project Settings.


#include "Tank.h"
#include "Components/ActorComponent.h"
#include "Perception/PawnSensingComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "TankController.h"
#include "Components/CapsuleComponent.h"
#include "FirstPersonCharacter.h"
#include "Net/UnrealNetwork.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "HealthBarWidget.h"
#include "Components/WidgetComponent.h"

// Sets default values
ATank::ATank()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("Pawn Sensing Component"));
	PawnSensingComp->SetPeripheralVisionAngle(150.f);

	TriggerComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Attack Range"));
	TriggerComponent->SetupAttachment(RootComponent);

	WidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("HealthBar"));
	WidgetComponent->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ATank::BeginPlay()
{
	Super::BeginPlay();

	if(PawnSensingComp)
	{
		PawnSensingComp->OnSeePawn.AddDynamic(this, &ATank::OnPlayerCaught);
	}

	if (TriggerComponent) 
	{
		TriggerComponent->OnComponentBeginOverlap.AddDynamic(this, &ATank::StartAttackPlayer);
		TriggerComponent->OnComponentEndOverlap.AddDynamic(this, &ATank::EndAttackPlayer);
	}

	bReplicates = true;

	ATankController* temp = Cast<ATankController>(GetController());
	this->SetOwner(temp);

	HealthBar = Cast<UHealthBarWidget>(WidgetComponent->GetUserWidgetObject());
}

void ATank::StartAttackPlayer(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AFirstPersonCharacter* player = Cast<AFirstPersonCharacter>(OtherActor);

	if (player) {
		bAttackPlayer = true;
		//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, TEXT("Player entered attack range"));

		if (!AttackTimer.IsValid() || !GetWorldTimerManager().IsTimerActive(AttackTimer)) {
			DamagePlayer(player);
		}
	}
}

void ATank::DamagePlayer(AFirstPersonCharacter* player)
{
	if (bAttackPlayer) {
		//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, TEXT("Hitting player"));
		player->TakeDamage(DamageDealt);
		FTimerDelegate TimerDel;
		TimerDel.BindUFunction(this, FName("DamagePlayer"), player);
		GetWorldTimerManager().SetTimer(AttackTimer, TimerDel, AttackSpeed, false);
	}
}

void ATank::EndAttackPlayer(class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	AFirstPersonCharacter* player = Cast<AFirstPersonCharacter>(OtherActor);

	if (player) {
		//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, TEXT("Player left attack range"));
		bAttackPlayer = false;
	}	
}

void ATank::TakeDamage(float DamageValue)
{
	Health -= DamageValue;
	if (Health > MaxHealth) {
		Health = MaxHealth;
	}

	UpdateHealthBar();

	if (Health <= 0)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Tank Taking Damage"));
		Server_EnemyDied();
	}
}


void ATank::OnRep_Health()
{
	UpdateHealthBar();
}

void ATank::UpdateHealthBar() {
	if (HealthBar) {
		HealthBar->SetPercent(Health / MaxHealth);
		if (HealthBar->GetPercent() < 1) {
			HealthBar->SetVisible(true);
		}
		else {
			HealthBar->SetVisible(false);
		}
	}
}

void ATank::EnemyDied()
{
	this->Destroy();
}

// Called every frame
void ATank::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ATank::OnPlayerCaught(APawn* Pawn) 
{
	ATankController* AIController = Cast<ATankController>(GetController());

	if (AIController)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("The tank has seen you!"));
		AIController->SetPlayerCaught(Pawn);
	}
}

void ATank::NetMulticast_EnemyDied_Implementation()
{
	if (GEngine)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Tank Died"));
	}
	this->Destroy();
}

void ATank::Server_EnemyDied_Implementation()
{
	if (GEngine)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Tank Died"));
	}
	//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, TEXT("Server destroying tank"));
	this->Destroy();
}

void ATank::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATank, Health);
}



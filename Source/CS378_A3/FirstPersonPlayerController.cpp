// Fill out your copyright notice in the Description page of Project Settings.


#include "FirstPersonPlayerController.h"
#include "FirstPersonCharacter.h"

const FName AFirstPersonPlayerController::ForwardBinding("Forward");
const FName AFirstPersonPlayerController::StrafeBinding("Strafe");
const FName AFirstPersonPlayerController::CameraYawBinding("CameraYaw");
const FName AFirstPersonPlayerController::CameraPitchBinding("CameraPitch");

const FName AFirstPersonPlayerController::FireBinding("Fire");

void AFirstPersonPlayerController::BeginPlay() {
    InputYawScale = 1;
    InputPitchScale = 1;
}

void AFirstPersonPlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();

    // Bind movement bindings
    InputComponent->BindAxis(ForwardBinding, this, &AFirstPersonPlayerController::Move);
    InputComponent->BindAxis(StrafeBinding, this, &AFirstPersonPlayerController::Strafe);

    // Bind camera bindings
    InputComponent->BindAxis(CameraYawBinding, this, &AFirstPersonPlayerController::AddCameraYaw);
    InputComponent->BindAxis(CameraPitchBinding, this, &AFirstPersonPlayerController::AddCameraPitch);

    InputComponent->BindAction(FireBinding, IE_Pressed, this, &AFirstPersonPlayerController::StartFiring);
    InputComponent->BindAction(FireBinding, IE_Released, this, &AFirstPersonPlayerController::StopFiring);
}

void AFirstPersonPlayerController::Move(float value)
{
    AFirstPersonCharacter* character = Cast<AFirstPersonCharacter>(this->GetCharacter());
    if (character) {
        character->Move(value);
    }
}

void AFirstPersonPlayerController::Strafe(float value)
{
    AFirstPersonCharacter* character = Cast<AFirstPersonCharacter>(this->GetCharacter());
    if (character){
        character->Strafe(value);
    }
    
}

void AFirstPersonPlayerController::AddCameraPitch(float value)
{
    AFirstPersonCharacter* character = Cast<AFirstPersonCharacter>(this->GetCharacter());
    if (character) {
        character->CameraPitch(value);
    }
}

void AFirstPersonPlayerController::AddCameraYaw(float value)
{
    AFirstPersonCharacter* character = Cast<AFirstPersonCharacter>(this->GetCharacter());
    if (character) {
        character->CameraYaw(value);
    }
}

void AFirstPersonPlayerController::StartFiring()
{
    AFirstPersonCharacter* character = Cast<AFirstPersonCharacter>(this->GetCharacter());
    if (character) {
        character->StartFiring();
    }
}

void AFirstPersonPlayerController::StopFiring()
{
    AFirstPersonCharacter* character = Cast<AFirstPersonCharacter>(this->GetCharacter());
    if (character) {
        character->StopFiring();
    }
}

void AFirstPersonPlayerController::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);

    TArray<AActor*> ActorList;

    GetSeamlessTravelActorList(true, ActorList);

    ActorList.Add(InPawn);
}

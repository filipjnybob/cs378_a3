// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "PickRandomTaskNode.generated.h"

/**
 * 
 */
UCLASS()
class CS378_A3_API UPickRandomTaskNode : public UBTTaskNode
{
	GENERATED_BODY()

private:
	EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComponent, uint8* NodeMemory);
	
};

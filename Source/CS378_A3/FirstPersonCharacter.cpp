// Fill out your copyright notice in the Description page of Project Settings.

#include "FirstPersonCharacter.h"
#include "Camera/CameraComponent.h"
#include "Net/UnrealNetwork.h"
#include "FirstPersonPlayerController.h"
#include "DrawDebugHelpers.h"
#include "Tank.h"
#include "Ranged.h"
#include "Components/WidgetComponent.h"
#include "HealthBarWidget.h"

// Sets default values
AFirstPersonCharacter::AFirstPersonCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(RootComponent);
	CameraComponent->bUsePawnControlRotation = true;

	WidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("HealthBar"));
	WidgetComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AFirstPersonCharacter::BeginPlay()
{
	Super::BeginPlay();

	HealthBar = Cast<UHealthBarWidget>(WidgetComponent->GetUserWidgetObject());
}

void AFirstPersonCharacter::ApplyMove(float scale)
{
	AddMovementInput(GetActorForwardVector(), scale);
}

void AFirstPersonCharacter::ApplyStrafe(float scale)
{
	AddMovementInput(GetActorRightVector(), scale);
}

void AFirstPersonCharacter::ApplyCameraYaw(float value)
{
	AddControllerYawInput(value);
}

void AFirstPersonCharacter::ApplyCameraPitch(float value)
{
	AddControllerPitchInput(value);
}

void AFirstPersonCharacter::FireStart()
{
}

void AFirstPersonCharacter::FireStop()
{
}

void AFirstPersonCharacter::Fire()
{
	FVector RaycastStart;
	FRotator PlayerRotation;
	GetController<AFirstPersonPlayerController>()->GetPlayerViewPoint(RaycastStart, PlayerRotation);

	float distance = 2500.f;
	FVector RaycastEnd = RaycastStart + PlayerRotation.Vector() * distance;

	FHitResult Hit;
	FCollisionQueryParams CollisionParams = FCollisionQueryParams::DefaultQueryParam;
	CollisionParams.AddIgnoredActor(this);

	bool Success = GetWorld()->LineTraceSingleByObjectType(Hit, RaycastStart, RaycastEnd, FCollisionObjectQueryParams(ECC_TO_BITFIELD(ECC_WorldStatic) | ECC_TO_BITFIELD(ECC_Pawn)), CollisionParams);

	// TODO: Draw line from a location determined by a variable
	RaycastStart += GetActorRightVector() * 10.f - GetActorUpVector() * 5.f;

	if (Success)
	{
		RaycastEnd = Hit.Location;
		ACharacter *EnemyCharacter = Cast<ACharacter>(Hit.GetActor());

		if (EnemyCharacter)
		{
			// GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Blue, TEXT("Hit Enemy"));
			Server_DamageEnemy(EnemyCharacter, GunDamage);
		}
	}

	DrawDebugLine(GetWorld(), RaycastStart, RaycastEnd, FColor::Yellow, false, 0.06f, '\000', 0.6f);
	NetMulticast_Shoot(RaycastStart, RaycastEnd);
	Server_Shoot(RaycastStart, RaycastEnd);
}

void AFirstPersonCharacter::Server_DamageEnemy_Implementation(ACharacter *Enemy, float Damage)
{
	if (Enemy->IsA(ATank::StaticClass()))
	{
		ATank *Tank = Cast<ATank>(Enemy);
		Tank->TakeDamage(Damage);
	}
	else if (Enemy->IsA(ARanged::StaticClass()))
	{
		ARanged *Ranged = Cast<ARanged>(Enemy);
		Ranged->TakeDamage(Damage);
	}
}

bool AFirstPersonCharacter::Server_DamageEnemy_Validate(ACharacter *Enemy, float Damage)
{
	return true;
}

void AFirstPersonCharacter::OnRep_Health()
{
	if (HealthBar) {
		HealthBar->SetPercent(Health / MaxHealth);
	}
}

void AFirstPersonCharacter::TakeDamage(float DamageValue)
{
	Health-=DamageValue;

	if (Health > MaxHealth) {
		Health = MaxHealth;
	}

	if (HealthBar) {
		HealthBar->SetPercent(Health / MaxHealth);
	}
	if (Health <= 0)
	{
		if (HasAuthority())
		{
			NetMulticast_PlayerDied();
		}
	}
}

// Called every frame
void AFirstPersonCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AFirstPersonCharacter::SetupPlayerInputComponent(UInputComponent *PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AFirstPersonCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AFirstPersonCharacter, Health);
}

// void AFirstPersonCharacter::Server_PlayerDied(int Health)
// {
// 	Server_PlayerDied_Implementation(Health);
// }
/*
void AFirstPersonCharacter::Server_PlayerDied(int CurrentHealth)
{
	Server_PlayerDied_Implementation(CurrentHealth);
}*/

void AFirstPersonCharacter::PlayerDied()
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("You Died"));
	}
}

void AFirstPersonCharacter::Server_PlayerDied_Implementation(int32 CurrentHealth)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("A Player Died"));
	}

	this->Destroy();
}

bool AFirstPersonCharacter::Server_PlayerDied_Validate(int32 CurrentHealth)
{
	if (CurrentHealth <= 0)
	{
		return true;
	}
	return false;
}

void AFirstPersonCharacter::NetMulticast_PlayerDied_Implementation()
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("You Died"));
	}
	this->Destroy();
}

void AFirstPersonCharacter::Server_Shoot_Implementation(FVector RayCastStart, FVector RayCastEnd)
{
	DrawDebugLine(GetWorld(), RayCastStart, RayCastEnd, FColor::Yellow, false, 0.06f, '\000', 1.2f);
}

bool AFirstPersonCharacter::Server_Shoot_Validate(FVector RayCastStart, FVector RayCastEnd)
{
	return true;
}

void AFirstPersonCharacter::NetMulticast_Shoot_Implementation(FVector RayCastStart, FVector RayCastEnd)
{
	DrawDebugLine(GetWorld(), RayCastStart, RayCastEnd, FColor::Yellow, false, 0.06f, '\000', 1.2f);
}
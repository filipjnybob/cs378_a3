// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CS378_A3GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CS378_A3_API ACS378_A3GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ACS378_A3GameModeBase();
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "LevelTransitionVolume.generated.h"

/**
 * 
 */
UCLASS()
class CS378_A3_API ALevelTransitionVolume : public ATriggerBox
{
	GENERATED_BODY()

protected:
	UFUNCTION()
		void OnOverlap(AActor* OverlappedActor, AActor* OtherActor);

	virtual void BeginPlay() override;

	UFUNCTION(Server, Reliable)
	void Server_SwapLevel();
	void Server_SwapLevel_Implementation();

};

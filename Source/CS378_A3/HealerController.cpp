// Fill out your copyright notice in the Description page of Project Settings.


#include "HealerController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Tank.h"
#include "Healer.h"

AHealerController::AHealerController() {

	HealTargetKey = "HealTarget";
}

void AHealerController::SetPlayerCaught(APawn* InPawn) {
	ATank* Tank = Cast<ATank>(InPawn);
	
	if (Tank) {
		//GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, "Spotted Tank");
		if (Tank->GetHealth() < Tank->GetMaxHealth()) {
			
			if (BlackboardComponent)
			{
				ATank* CurrentTarget = (ATank*)BlackboardComponent->GetValueAsObject(HealTargetKey);
				if (CurrentTarget) {
					FVector CurrentLocation = GetCharacter()->GetActorLocation();
					FVector TargetLocation = CurrentTarget->GetActorLocation();
					FVector NewTargetLocation = InPawn->GetActorLocation();

					// Healer heals closest enemy without full health

					if (FVector::Distance(CurrentLocation, NewTargetLocation) < FVector::Distance(CurrentLocation, TargetLocation)) {
						BlackboardComponent->SetValueAsObject(HealTargetKey, InPawn);
					}
				}
				else {
					BlackboardComponent->SetValueAsObject(HealTargetKey, InPawn);
				}


				//BlackboardComponent->SetValueAsBool(SeePlayerKey, true);
			}
		}
	}
}

void AHealerController::Heal(AActor* Target)
{
	ATank* Tank = Cast<ATank>(Target);

	if (Tank) {
		AHealer* character = Cast<AHealer>(GetCharacter());
		if (character) {
			if (character->Heal(Tank)) {
				// Target was fully healed
				BlackboardComponent->ClearValue(HealTargetKey);
			}
		}
	}
}

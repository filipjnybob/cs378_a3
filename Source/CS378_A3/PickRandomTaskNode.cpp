// Fill out your copyright notice in the Description page of Project Settings.


#include "PickRandomTaskNode.h"
#include "TankController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "NavigationSystem.h"

EBTNodeResult::Type UPickRandomTaskNode::ExecuteTask(UBehaviorTreeComponent & OwnerComponent, uint8* NodeMemory)
{   
    ATankController* AIController = Cast<ATankController>(OwnerComponent.GetAIOwner());

    if (AIController)
    {
        UBlackboardComponent* BlackboardComponent = AIController->GetBlackboardComponent();

        UNavigationSystemV1* Nav = UNavigationSystemV1::GetCurrent(GetWorld());
        FNavLocation Result;
        FVector Location = AIController->GetPawn()->GetActorLocation();

        bool Success = Nav->GetRandomReachablePointInRadius(Location, AIController->GetWanderRadius(), Result);

        if(!Success) {
            return EBTNodeResult::Failed;
        }

        BlackboardComponent->SetValueAsVector("Destination", Result.Location);
    
        return EBTNodeResult::Succeeded;
    }
    return EBTNodeResult::Failed;
}


// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "FirstPersonPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CS378_A3_API AFirstPersonPlayerController : public APlayerController
{
	GENERATED_BODY()

protected:

	virtual void BeginPlay() override;

	// Axis bindings
	static const FName ForwardBinding;
	static const FName StrafeBinding;
	static const FName CameraPitchBinding;
	static const FName CameraYawBinding;

	// Action bindings
	static const FName FireBinding;

	virtual void SetupInputComponent() override;

private:
	UFUNCTION()
		void Move(float value);

	UFUNCTION()
		void Strafe(float value);	

	UFUNCTION()
		void AddCameraPitch(float value);

	UFUNCTION()
		void AddCameraYaw(float value);

	UFUNCTION()
		void StartFiring();

	UFUNCTION()
		void StopFiring();

	virtual void OnPossess(APawn* InPawn) override;
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthPack.h"
#include "Components/CapsuleComponent.h"
#include "FirstPersonCharacter.h"

// Sets default values
AHealthPack::AHealthPack()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	MeshComponent->SetupAttachment(RootComponent);

	TriggerComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger"));
	TriggerComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AHealthPack::BeginPlay()
{
	Super::BeginPlay();
	
	TriggerComponent->OnComponentBeginOverlap.AddDynamic(this, &AHealthPack::OnOverlapBegin);
}

void AHealthPack::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AFirstPersonCharacter* Player = Cast<AFirstPersonCharacter>(OtherActor);

	if (Player) {
		if (Player->GetHealth() < Player->GetMaxHealth()) {
			Player->TakeDamage(-HealingAmount);
			this->Destroy();
		}
	}
}

// Called every frame
void AHealthPack::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tank.h"
#include "Healer.generated.h"

/**
 * 
 */
UCLASS()
class CS378_A3_API AHealer : public ATank
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float HealValue = 30.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float HealFrequency = 0.6f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float HealRange = 300.0f;

public:
	UFUNCTION()
		bool Heal(ATank* target);
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "PickLocationAwayFromTargetTask.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "RangedController.h"

EBTNodeResult::Type UPickLocationAwayFromTargetTask::ExecuteTask(UBehaviorTreeComponent& OwnerComponent, uint8* NodeMemory) {

    ARangedController* AIController = Cast<ARangedController>(OwnerComponent.GetAIOwner());

    if (AIController) {
        UBlackboardComponent* BlackboardComponent = AIController->GetBlackboardComponent();

        FVector CurrentLocation = AIController->GetPawn()->GetActorLocation();
        AActor* Target = (AActor*)BlackboardComponent->GetValueAsObject(AIController->PlayerLocationKey);

        if (Target)
        {
            FVector TargetLocation = Target->GetActorLocation();
            FVector dirVector = (CurrentLocation - TargetLocation);
            dirVector.Normalize(1);
            BlackboardComponent->SetValueAsVector(AIController->NormPlayerLocationKey, TargetLocation + dirVector * 500);

            return EBTNodeResult::Succeeded;
        }
      
    }

    return EBTNodeResult::Failed;
    
}
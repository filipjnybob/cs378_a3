// Fill out your copyright notice in the Description page of Project Settings.

#include "Ranged.h"
#include "Components/ActorComponent.h"
#include "Perception/PawnSensingComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "RangedController.h"
#include "Components/CapsuleComponent.h"
#include "FirstPersonCharacter.h"
#include "Net/UnrealNetwork.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"
#include "Projectile.h"
#include "Tank.h"
#include "HealthBarWidget.h"
#include "Components/WidgetComponent.h"

// Sets default values
ARanged::ARanged()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("Pawn Sensing Component"));
	PawnSensingComp->SetPeripheralVisionAngle(150.f);

	WidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("HealthBar"));
	WidgetComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ARanged::BeginPlay()
{
	Super::BeginPlay();

	if (PawnSensingComp)
	{
		PawnSensingComp->OnSeePawn.AddDynamic(this, &ARanged::OnPlayerCaught);

		// PawnSensingComp->OnComponentBeginOverlap.AddDynamic(this, &ARanged::StartAttackPlayer);
		// PawnSensingComp->OnComponentEndOverlap.AddDynamic(this, &ARanged::EndAttackPlayer);
	}
	bReplicates = true;

	ARangedController *temp = Cast<ARangedController>(GetController());
	this->SetOwner(temp);

	HealthBar = Cast<UHealthBarWidget>(WidgetComponent->GetUserWidgetObject());
}

void ARanged::StartAttackPlayer(APawn *Pawn)
{
	if (Pawn)
	{
		if (!bAttackPlayer)
		{
			bAttackPlayer = true;
			AFirstPersonCharacter* player = Cast<AFirstPersonCharacter>(Pawn);
			Server_GaugePlayer(player);
		}
	}
}

void ARanged::Server_GaugePlayer_Implementation(AFirstPersonCharacter *player)
{
	//GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("Entered GaugePlayer"));
	if (bAttackPlayer)
	{
		FVector RaycastStart;
		FRotator AIRotation;
		AIRotation = GetActorRotation();
		RaycastStart = GetActorLocation();

		float distance = 2000.f;
		FVector RaycastEnd = RaycastStart + AIRotation.Vector() * distance;

		FHitResult Hit;
		FCollisionQueryParams CollisionParams = FCollisionQueryParams::DefaultQueryParam;
		CollisionParams.AddIgnoredActor(this);
		// CollisionParams.AddIgnoredActor(ATank);

		bool Success = GetWorld()->LineTraceSingleByObjectType(Hit, RaycastStart, RaycastEnd, FCollisionObjectQueryParams(ECC_TO_BITFIELD(ECC_WorldStatic) | ECC_TO_BITFIELD(ECC_Pawn)), CollisionParams);
		// good

		// TODO: Draw line from a location determined by a variable
		RaycastStart += GetActorRightVector() * 10.f - GetActorUpVector() * 5.f;

		if (Success)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("Hit!"));
			DamagePlayer(player, Hit, RaycastStart, AIRotation);
		}

		FTimerDelegate TimerDel;
		TimerDel.BindUFunction(this, FName("Server_GaugePlayer"), player);
		GetWorldTimerManager().SetTimer(AttackTimer, TimerDel, AttackSpeed, false);
		// DrawDebugLine(GetWorld(), RaycastStart, RaycastEnd, FColor::Yellow, false, 0.06f, '\000', 0.6f);
	}
}

void ARanged::DamagePlayer(AFirstPersonCharacter *player, FHitResult Hit, FVector RaycastStart, FRotator AIRotation)
{
	FVector RaycastEnd = Hit.Location;
	UWorld *World = GetWorld();
	if (World)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("Entering DamagePlayer!"));
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;
		SpawnParams.Instigator = GetInstigator();
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

		AProjectile *Projectile = World->SpawnActor<AProjectile>(ProjectileClass, RaycastStart + GetActorForwardVector() * 25, AIRotation, SpawnParams);

		if (Projectile)
		{
			FVector LaunchDirection = AIRotation.Vector();
			Projectile->FireInDirection(LaunchDirection);
			//GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Blue, TEXT("Launched!"));
		}
	}
}

void ARanged::TakeDamage(float DamageValue)
{
	Health -= DamageValue;

	if (Health > MaxHealth) {
		Health = MaxHealth;
	}

	UpdateHealthBar();
	if (Health <= 0)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Ranged Taking Damage"));
		Server_EnemyDied();
	}
}

void ARanged::OnRep_Health()
{
	UpdateHealthBar();
}

void ARanged::UpdateHealthBar()
{
	if (HealthBar) {
		HealthBar->SetPercent(Health / MaxHealth);
		if (HealthBar->GetPercent() < 1) {
			HealthBar->SetVisible(true);
		}
		else {
			HealthBar->SetVisible(false);
		}
	}
}

void ARanged::EnemyDied()
{
	this->Destroy();
}

// Called every frame
void ARanged::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ARanged::SetupPlayerInputComponent(UInputComponent *PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ARanged::OnPlayerCaught(APawn *Pawn)
{
	ARangedController *AIController = Cast<ARangedController>(GetController());

	if (AIController)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Calling OnPlayerCaught"));
		AIController->SetPlayerCaught(Pawn);
		StartAttackPlayer(Pawn);
	}
}

void ARanged::Server_EnemyDied_Implementation()
{
	if (GEngine)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Ranged Attacker Died"));
	}
	//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, TEXT("Server destroying Ranged Attacker"));
	this->Destroy();
}

void ARanged::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ARanged, Health);
}

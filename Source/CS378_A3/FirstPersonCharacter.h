// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Tank.h"
#include "HealthBarWidget.h"
#include "FirstPersonCharacter.generated.h"

UCLASS()
class CS378_A3_API AFirstPersonCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AFirstPersonCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void ApplyMove(float scale);

	UFUNCTION(BlueprintCallable)
	void ApplyStrafe(float scale);

	UFUNCTION(BlueprintCallable)
	void ApplyCameraYaw(float value);

	UFUNCTION(BlueprintCallable)
	void ApplyCameraPitch(float value);

	UFUNCTION(BlueprintCallable)
	void FireStart();

	UFUNCTION(BlueprintCallable)
	void FireStop();

	UFUNCTION(BlueprintCallable)
	void Fire();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UCameraComponent *CameraComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxHealth = 100;

	UPROPERTY(ReplicatedUsing = OnRep_Health, EditAnywhere, BlueprintReadWrite)
	float Health = MaxHealth;

	UFUNCTION()
	void OnRep_Health();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float GunDamage = 8.f;

	UFUNCTION()
	void PlayerDied();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_PlayerDied(int32 CurrentHealth);
	void Server_PlayerDied_Implementation(int32 CurrentHealth);
	bool Server_PlayerDied_Validate(int32 CurrentHealth);

	UFUNCTION(NetMulticast, Reliable)
	void NetMulticast_PlayerDied();
	void NetMulticast_PlayerDied_Implementation();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_DamageEnemy(ACharacter *Enemy, float Damage);
	void Server_DamageEnemy_Implementation(ACharacter *Enemy, float Damage);
	bool Server_DamageEnemy_Validate(ACharacter *Enemy, float Damage);

	UPROPERTY()
		UHealthBarWidget* HealthBar;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UWidgetComponent* WidgetComponent;

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_Shoot(FVector RayCastStart, FVector RayCastEnd);
	void Server_Shoot_Implementation(FVector RayCastStart, FVector RayCastEnd);
	bool Server_Shoot_Validate(FVector RayCastStart, FVector RayCastEnd);

	UFUNCTION(NetMulticast, Reliable)
	void NetMulticast_Shoot(FVector RayCastStart, FVector RayCastEnd);
	void NetMulticast_Shoot_Implementation(FVector RayCastStart, FVector RayCastEnd);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) override;

	UFUNCTION(BlueprintImplementableEvent)
	void Move(float value);

	UFUNCTION(BlueprintImplementableEvent)
	void Strafe(float value);

	UFUNCTION(BlueprintImplementableEvent)
	void CameraYaw(float value);

	UFUNCTION(BlueprintImplementableEvent)
	void CameraPitch(float value);

	UFUNCTION(BlueprintImplementableEvent)
	void StartFiring();

	UFUNCTION(BlueprintImplementableEvent)
	void StopFiring();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const override;

	UFUNCTION()
	void TakeDamage(float DamageValue);

	FORCEINLINE float GetHealth() const { return Health; }
	FORCEINLINE float GetMaxHealth() const { return MaxHealth; }
};

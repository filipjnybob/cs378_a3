// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelTransitionVolume.h"
#include "FirstPersonCharacter.h"
#include "Engine/World.h"
#include "Engine.h"

void ALevelTransitionVolume::BeginPlay() {
	OnActorBeginOverlap.AddDynamic(this, &ALevelTransitionVolume::OnOverlap);
}

void ALevelTransitionVolume::Server_SwapLevel_Implementation()
{
	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Transitioning level..."));
	FString LevelName = GetWorld()->GetMapName();
	LevelName.RemoveFromStart(GetWorld()->StreamingLevelsPrefix);
	if (LevelName.Equals("level1")) {
		GetWorld()->ServerTravel("/Game/Maps/Level2?listen", true);
	}
	else {
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Travel to level 1"));
		GetWorld()->ServerTravel("/Game/Maps/level1?listen", true);
	}
	
}

void ALevelTransitionVolume::OnOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Overlap"));
	if (OtherActor->IsA(AFirstPersonCharacter::StaticClass())) {
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Player Overlap"));
		Server_SwapLevel();
	}
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "Healer.h"
#include "Tank.h"

bool AHealer::Heal(ATank* target)
{
	//GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Green, "Healed enemy");
	target->TakeDamage(-HealValue);

	if (target->GetHealth() >= target->GetMaxHealth()) {
		return true;
	}
	return false;
}

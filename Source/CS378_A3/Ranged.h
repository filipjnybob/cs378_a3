#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Ranged.generated.h"

UCLASS()
class CS378_A3_API ARanged : public ACharacter
{
	GENERATED_BODY()

	UFUNCTION()
	void UpdateHealthBar();
public:
	// Sets default values for this character's properties
	ARanged();

	FORCEINLINE class UBehaviorTree *GetBehaviorTree() const { return BehaviorTree; }
	FORCEINLINE class TArray<AActor *> GetPatrolPoints() const
	{
		return PatrolPoints;
	}

	UPROPERTY(VisibleAnywhere, Category = AI) class UPawnSensingComponent *PawnSensingComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UCapsuleComponent *TriggerComponent;

	UFUNCTION()
	void TakeDamage(float DamageValue);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	class UBehaviorTree *BehaviorTree;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<AActor *> PatrolPoints;

	UFUNCTION()
	void StartAttackPlayer(APawn *Pawn);

	UFUNCTION(Server, Reliable)
	void Server_GaugePlayer(class AFirstPersonCharacter *player);
	void Server_GaugePlayer_Implementation(class AFirstPersonCharacter *player);

	UFUNCTION()
	void DamagePlayer(AFirstPersonCharacter *player, FHitResult Hit, FVector RaycastStart, FRotator AIRotation);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxHealth = 100;

	UPROPERTY(ReplicatedUsing = OnRep_Health, EditAnywhere, BlueprintReadWrite)
	float Health = MaxHealth;

	UFUNCTION()
	void OnRep_Health();

	UPROPERTY()
	FTimerHandle AttackTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float AttackSpeed = 1.0f;

	UPROPERTY()
	bool bAttackPlayer = false;

	UFUNCTION()
	void EnemyDied();

	UFUNCTION(Server, Reliable)
	void Server_EnemyDied();
	void Server_EnemyDied_Implementation();

	// Projectile class to spawn.
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AProjectile> ProjectileClass;

	UPROPERTY()
		class UHealthBarWidget* HealthBar;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UWidgetComponent* WidgetComponent;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const override;

private:
	UFUNCTION()
	void OnPlayerCaught(APawn *Pawn);
};
// Fill out your copyright notice in the Description page of Project Settings.

#include "RangedController.h"
#include "Ranged.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Math/Vector.h"

ARangedController::ARangedController()
{
    BehaviorComponent = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("Behavior Tree Component"));

    BlackboardComponent = CreateDefaultSubobject<UBlackboardComponent>(TEXT("Blackboard Component"));

    DestinationKey = "Destination";
    PlayerLocationKey = "PlayerLocation";
    NormPlayerLocationKey = "NormPlayerLocation";
    SeePlayerKey = "CanSeePlayer";
    TooCloseKey = "TooClose";
}

void ARangedController::SetPlayerCaught(APawn *InPawn)
{
    if (BlackboardComponent)
    {
        BlackboardComponent->SetValueAsBool(SeePlayerKey, true);

        AActor *CurrentTarget = (AActor *)BlackboardComponent->GetValueAsObject(PlayerLocationKey);
        if (CurrentTarget)
        {

            FVector CurrentLocation = GetCharacter()->GetActorLocation();
            FVector TargetLocation = CurrentTarget->GetActorLocation();
            FVector NewTargetLocation = InPawn->GetActorLocation();

            // Ranged chases closest player

            if (FVector::Distance(CurrentLocation, NewTargetLocation) < FVector::Distance(CurrentLocation, TargetLocation))
            {
                BlackboardComponent->SetValueAsObject(PlayerLocationKey, InPawn);
            }
            //SetTooClose(CurrentTarget);

        }
        else
        {
            BlackboardComponent->SetValueAsObject(PlayerLocationKey, InPawn);
        }
       
    }
}

void ARangedController::SetTooClose(AActor* Target)
{
    FVector CurrentLocation = GetCharacter()->GetActorLocation();
    FVector TargetLocation = Target->GetActorLocation();

    if (FVector::Distance(CurrentLocation, TargetLocation) < 300)
    {
        GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, TEXT("Too Close!"));
        BlackboardComponent->SetValueAsBool(TooCloseKey, true);

        FVector dirVector = (CurrentLocation - TargetLocation);

        dirVector.Normalize(1);

        BlackboardComponent->SetValueAsVector(NormPlayerLocationKey, TargetLocation + dirVector * 500);
    }
    else
    {
        BlackboardComponent->SetValueAsBool(TooCloseKey, false);
    }
}

void ARangedController::OnPossess(APawn *InPawn)
{
    Super::OnPossess(InPawn);

    ARanged *Ranged = Cast<ARanged>(InPawn);

    if (BlackboardComponent)
    {
        if (Ranged && Ranged->GetBehaviorTree())
        {
            if (Ranged->GetBehaviorTree()->BlackboardAsset)
            {
                BlackboardComponent->InitializeBlackboard(*(Ranged->GetBehaviorTree()->BlackboardAsset));
            }

            if (BehaviorComponent)
            {
                BehaviorComponent->StartTree(*Ranged->GetBehaviorTree());
            }
        }
    }
}
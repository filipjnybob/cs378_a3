// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthBarWidget.h"
#include "Components/ProgressBar.h"

void UHealthBarWidget::SetPercent(float Percent)
{
	if (HealthBar) {
		HealthBar->SetPercent(FMath::Clamp(Percent, 0.0f, 1.0f));
	}
}

float UHealthBarWidget::GetPercent() {
	if (HealthBar) {
		return HealthBar->Percent;
	}

	return -1.0f;
}

void UHealthBarWidget::SetVisible(bool Visible)
{
	if (HealthBar) {
		if (Visible) {
			HealthBar->SetVisibility(ESlateVisibility::Visible);
		}
		else {
			HealthBar->SetVisibility(ESlateVisibility::Hidden);
		}
	}
}

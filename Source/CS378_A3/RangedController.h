// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "RangedController.generated.h"

/**
 *
 */
UCLASS()
class CS378_A3_API ARangedController : public AAIController
{
	UPROPERTY()
	class UBehaviorTreeComponent *BehaviorComponent;

	UPROPERTY()
	class UBlackboardComponent *BlackboardComponent;

	virtual void OnPossess(APawn *InPawn) override;

	// Blackboard keys
	UPROPERTY(EditDefaultsOnly, Category = AI)
	FName DestinationKey; // Location/point for the AI to go to

public:
	UPROPERTY(EditDefaultsOnly, Category = AI)
	FName PlayerLocationKey; // reference to the players location

	UPROPERTY(EditDefaultsOnly, Category = AI)
	FName NormPlayerLocationKey;
private:

	UPROPERTY(EditDefaultsOnly, Category = AI)
	FName SeePlayerKey; // reference to whether or not the AI can see the player

	UPROPERTY(EditDefaultsOnly, Category = AI)
	FName TooCloseKey; // reference to whether or not the AI is within the radius of the player to start running away.

	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float WanderRadius = 300.f;

public:
	ARangedController();

	FORCEINLINE class UBlackboardComponent *GetBlackboardComponent() const { return BlackboardComponent; }
	FORCEINLINE int GetWanderRadius() const { return WanderRadius; }

	UFUNCTION()
	void SetPlayerCaught(APawn *InPawn);

	UFUNCTION()
	void SetTooClose(AActor* Target);
};

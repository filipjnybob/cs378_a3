// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Controller.h"
#include "AIController.h"
#include "TankController.generated.h"

/**
 * 
 */
UCLASS()
class CS378_A3_API ATankController : public AAIController
{
protected:
	UPROPERTY()
	class UBehaviorTreeComponent *BehaviorComponent;

	UPROPERTY()
	class UBlackboardComponent* BlackboardComponent;

private:
	virtual void OnPossess(APawn* InPawn) override; 

	// Blackboard keys
	UPROPERTY(EditDefaultsOnly, Category = AI)
	FName DestinationKey; // Location/point for the AI to go to

	UPROPERTY(EditDefaultsOnly, Category = AI)
	FName PlayerLocationKey; // reference to the players location

	UPROPERTY(EditDefaultsOnly, Category = AI)
	FName SeePlayerKey; // reference to whether or not the AI can see the player

	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float WanderRadius = 300.f;

public:
	ATankController();

	FORCEINLINE class UBlackboardComponent* GetBlackboardComponent() const { return BlackboardComponent; }
	FORCEINLINE int GetWanderRadius() const { return WanderRadius; }

	UFUNCTION()
	virtual void SetPlayerCaught(APawn *InPawn);
};

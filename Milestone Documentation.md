# Assignment 3 Milestone

## Overall Game Design

- Cooperative PVE shooter
- Moving / Camera mechanics
- Aiming mouse, shooting
- Player health
- AI that also incorporates the above 3 features
- Multiple players will work together to defeat AI players.
- 3 types of AI:
    - Healer- prioritizes walking towards and healing other AI’s over killing player if the other AI’s are low on health.
    - Tank- high health, low damage. Once it sees the player it prioritizes on getting close to player 
    - Ranged- keeps distance and focuses on shooting player with slower projectile speed.

## Software architecture:
- Foundation code in C++. Configurable features implemented in Blueprint.
- Keep player controller implementation model from Assignment 2 (Controller calls player-specific controls in Blueprint).
- Replicate player health using RPCs. Also replicate AI health to players.
- Player-character implementation will take 2 hours.
- Each AI type implementation will take ~4 hours

## Division of Labor:
- Character implementation and health UI
Pair Programming: Isaac, Minh, Byungik

- Tank AI implementation
Pair Programming: Isaac, Minh, Byungik, Peter

- Healer AI implementation
Pair Programming: Isaac and Minh

- Ranged AI implementation
Pair Programming: Peter and Byungik
	
